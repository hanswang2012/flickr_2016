# Flickr Search Demo

This demo use laravel create an app which generates image galleries in response to user searches, drawing content from Flickr using their REST API.

- The app will require registration before allowing users to conduct searches.
- The search results will be paginated and displayed as five results per page, and the user will be able to navigate to other pages.
- Each image will be displayed as a thumbnail. Clicking on the thumbnail will open a new page which shows the full-size image.
- The app will maintain and display a list of recent searches made by the user.
- The app will use MySQL as its user management db as well as search history log, for below reasons
	- Persistent since its on server side
	- Re-use user registration & login db
	- Small volumn traffic is acceptable & fast enough to use MySQL to load data
- The app contains several unit test, simply run phpunit in command line from project root directory


### Things to extend in future

- Use other layer of cache to record user historical search, e.g. redis, to increase scalability
- More css tweaks on responsive style for smaller screen to increase usability
- Write Flickr API class unit test which fits into TDD env, in such way create a service monitor checker in Unit test



### Installation

 - Clone the code (Of course)

 - place Nginx config, (need hosts file change to match up with server name, reason see below)
```sh
$ config/flickr.conf
```

 - 2 steps for DB restore
```sh
$ database/flickr_setup.sql
$ database/db_bak.sql
```

 - change Directory permission
```sh
$ chown -R www-data storage/ bootstrap/cache/
```

 - composer install

 - npm install

 - bower install

 - gulp install globally

 - compile the frontend
```sh
$ gulp --production
```
 - hit the site (my case here is http://www.flickrdemo.com/), woola~!

 - run to examine test cases at root directory
```sh
$ ./vendor/bin/phpunit
```


### System Requirement

 - PHP >= 5.5.9
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Mbstring PHP Extension
 - Tokenizer PHP Extension
 - Mysql Version 5.7.13-0ubuntu0.16.04.2
 - nginx version nginx/1.10.0
 - Since my local dev env has hosted multiple dev site, its easily for me to specify server name to be http://www.flickrdemo.com, and same rule needs to be set on OS hosts file.

