var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var bowerDir = '../../../bower_components/';

elixir(function(mix) {
    mix.sass([
        'app.scss'
    ], 'resources/assets/css/app.css')

    .styles([
        bowerDir + 'bootstrap/dist/css/bootstrap.min.css',
        'app.css'
    ])

    .scripts([
        bowerDir + 'jquery/dist/jquery.min.js',
        bowerDir + 'bootstrap/dist/js/bootstrap.min.js',
        bowerDir + 'twbs-pagination//jquery.twbsPagination.min.js',
        'frontend.js'
    ])

    .copy('bower_components/bootstrap/fonts', 'public/build/fonts')

    .version([
        'css/all.css',
        'js/all.js'
    ]);
});
