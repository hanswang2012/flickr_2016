CREATE DATABASE `flickr` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'demo'@'localhost' IDENTIFIED BY 'demopass';
GRANT ALL PRIVILEGES ON `flickr`.* TO 'demo'@'localhost';
FLUSH PRIVILEGES;