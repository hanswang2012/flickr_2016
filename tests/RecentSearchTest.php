<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecentSearchTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test insertion of recent search modal.
     *
     * @return void
     */
    public function testFactoryModelInsertion()
    {
    	$record = factory(App\RecentSearch::class)->create([
    		'term' => 'From Unit Test',
    	]);
        $this->seeInDatabase('recent_search', [
    		'term' => 'From Unit Test',
        ]);
        $this->seeInDatabase('users', [
    		'id' => $record->user_id,
        ]);
    }
    /**
     * Test sequential insertion of user & recent search modal.
     *
     * @return void
     */
    public function testSequentialInsertion()
    {
    	$user = factory(App\User::class)->create([
    		'name' => 'UnitTestUser',
    	]);
        $this->seeInDatabase('users', [
    		'name' => 'UnitTestUser',
        ]);
    	$record = factory(App\RecentSearch::class)->create([
    		'user_id' => $user->id,
    	]);
        $this->seeInDatabase('recent_search', [
    		'user_id' => $user->id,
        ]);
    }
}
