<?php

namespace App\Http\Controllers;

use App\Flickr;
use App\RecentSearch;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searchRecords = RecentSearch::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')->get();
        return view('home', ['searchRecords' => $searchRecords]);
    }

    public function search(Request $request)
    {
        if (!$request->has('term')) {
            return response()->json([
                'status'        => 'failed',
                'error_message' => 'Bad Request Parameter',
            ], 400);
        }

        $params = [];
        $term   = $request->input('term');
        if ($request->has('page') && $request->input('page') > 1) {
            $params['page'] = $request->input('page');
        } else {
            $recentSearch          = new RecentSearch;
            $recentSearch->user_id = Auth::id();
            $recentSearch->term    = $term;
            $recentSearch->save();
        }

        $res = Flickr::searchPhoto($term, $params);
        if ($res['stat'] != 'ok') {
            return response()->json([
                'status'        => 'failed',
                'error_message' => 'Error happened when querying Flickr API',
            ], 200);
        }

        $data = [
            'page'    => $res['photos']['page'],
            'pages'   => $res['photos']['pages'],
            'perpage' => $res['photos']['perpage'],
            'total'   => $res['photos']['total'],
            'images'  => [],
        ];
        foreach ($res['photos']['photo'] as $imgStats) {
            $img              = [];
            $img['title']     = $imgStats['title'];
            $format_url       = 'https://farm' . $imgStats['farm'] . '.staticflickr.com/' . $imgStats['server'] . '/' . $imgStats['id'] . '_' . $imgStats['secret'];
            $img['url']       = $format_url . '.jpg';
            $img['thumbnail'] = $format_url . '_s.jpg';
            $data['images'][] = $img;
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
        ], 200);
    }
}
