<?php

namespace App;

class Flickr
{
    private static $method    = 'flickr.photos.search';
    private static $urlRest   = "https://api.flickr.com/services/rest/?";
    private static $format    = "json";
    private static $perPage   = "5";

    public static function searchPhoto($text, $params = array())
    {
        $req['nojsoncallback'] = 1;
        $req['method']         = self::$method;
        $req['api_key']        = config('app.flickr.key');
        $req['format']         = self::$format;
        $req['per_page']       = self::$perPage;

        foreach ($params as $k => $v) {
            $req[$k] = $v;
        }
        $req['text'] = $text;

        $url = self::$urlRest . http_build_query($req);

        $response = file_get_contents($url);
        return json_decode($response, true);
    }
}
