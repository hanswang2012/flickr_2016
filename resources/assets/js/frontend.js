$(function() {
    $('#search input').on('keypress', function(e) {
        if (e.which == 13) {
            triggerSearch();
        }
    });
    $('#search button').on('click', function(e) {
        triggerSearch();
    });
});

function enterLoadingStatus() {
    $('#search button').addClass('disabled');
    $('#search input').prop('disabled', true);
    // clear error status & existing on page result
    $('#main_content .alert').hide();
    $('#resultSet').html('');
}

function exitLoadingStatus() {
    $('#search button').removeClass('disabled');
    $('#search input').prop('disabled', false);
}

function updateSearchStack() {
    var newRecord = '<li><div class="label label-info"><span class="glyphicon glyphicon-flash"></span>' + $('#search input').val() + '</div></li>';
    $('#sidebar ul').prepend(newRecord);
}

function processingErrorMessage(error_msg) {
    $('#main_content .alert').text('error_msg');
    $('#main_content .alert').show();
    $('#pagination').html('');
    $('#main_content h1').text('No Result Fetched yet.')
}

function triggerSearch(page) {
    enterLoadingStatus();
    var requestData = {
        'term': $('#search input').val()
    };
    if (page != undefined && page > 1) {
        requestData.page = page
    } else {
        updateSearchStack();
    }
    $.ajax({
        url: '/search',
        type: 'GET',
        dataType: 'json',
        data: requestData,
        cache: false,
        success: function(resp) {
            if (resp.status == 'success') {
                renderResult(resp.data);
            } else {
                processingErrorMessage(resp.error_message);
            }
        },
        error: function(resp) {
            processingErrorMessage(resp.responseText);
        },
        complete: function() {
            exitLoadingStatus();
        }
    });
}

function renderResult(data) {
    $('#main_content h1').text(data.total + ' Record Image(s) matched the search');
    var container = $('#resultSet');
    for (var i = data.images.length - 1; i >= 0; i--) {
        var imgLink = '<a class="image-result" href="' + data.images[i].url + '">' + '<img src="' + data.images[i].thumbnail + '" alt="' + data.images[i].title + '" class="img-thumbnail"></a>';
        container.append(imgLink);
    }
    if (data.pages > 0) {
        $('#pagination').twbsPagination({
            totalPages: data.pages,
            visiblePages: 7,
            initiateStartPageClick: false,
            onPageClick: function(event, page) {
                triggerSearch(page);
            }
        });
    }
}