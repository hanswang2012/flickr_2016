@extends('layouts.app')

@section('title')
Welcome
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    You need to register or login to use this app.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
