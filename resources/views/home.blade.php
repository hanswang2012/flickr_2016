@extends('layouts.app')

@section('title')
Search SPA
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div id="search" class="input-group input-group-lg">
                <input type="text" class="form-control" placeholder="Type Something to start Search">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <span>Go!</span>
                        <img src="/css/loader.gif">
                    </button>
                </span>
            </div>
            <div id="main_content">
                <h1>No Result Fetched yet.</h1>
                <div class="alert alert-danger" role="alert"></div>
                <div id="resultSet"></div>
                <hr>
                <div id="pagination"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div id="sidebar" data-spy="affix" data-offset-top="60">
                <h3 class="highlight">Recent Search <i class="glyphicon glyphicon-time pull-right"></i></h3>
                <ul class="nav nav-stacked">
                    @foreach ($searchRecords as $searchRecord)
                    <li>
                        <div class="label label-info">
                            <span class="glyphicon glyphicon-flash"></span>
                            {{ $searchRecord->term }}
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
